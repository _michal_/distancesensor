/*******************************************************************************/
/* plik nagl�wkowy LCD.h                                                       */
/*******************************************************************************/

#ifndef __LCD_H 
#define __LCD_H

/*********************** Specyfikacja sprzetowa ********************************/

/*------------------------- Liczba lini i znak�w ------------------------------*/

#define LineLen     16                  /* Width (in characters)               */
#define NumLines     2                  /* Hight (in lines)                    */

/*-------------------- LCD - linie interfejsu wyswietlacza  -------------------*/

/* Lista wyprowadzen uzytych do podlaczenia wyswietlacza LCD 2x16
   - DB4 = P1.24
   - DB5 = P1.25
   - DB6 = P1.26
   - DB7 = P1.27
   - E   = P1.31 (for V1 P1.30) daniel P.1.31
   - RW  = P1.29 daniel zwarte do masy GND
   - RS  = P1.28 daniel P.1.28                                                 */


#define PIN_E                 0x80000000
#define PIN_RW                0x20000000
#define PIN_RS                0x10000000
#define PINS_CTRL             0xB0000000	//wszystkie linie kontrolne - czyli raz jeszcze E, RW, RS
#define PINS_DATA             0x0F000000	// linie danych DB4 - DB7

//Definicja stan�w poszczeg�lnych wyprowadzen, w skladni uzyto operator ternarny 
//jest to operator wyrazenia warunkowego: a ? b : c. 
//Zwraca on b gdy a jest prawda lub c w przeciwnym wypadku.

/* pin E  ustaw 1 lub 0                                                      */
#define LCD_E(x)              ((x) ? (IOSET1 = PIN_E)  : (IOCLR1 = PIN_E) );

/* pin RW ustaw 1 lub 0                                                      */
#define LCD_RW(x)             ((x) ? (IOSET1 = PIN_RW) : (IOCLR1 = PIN_RW));

/* pin RS ustaw 1 lub 0                                                      */
#define LCD_RS(x)             ((x) ? (IOSET1 = PIN_RS) : (IOCLR1 = PIN_RS));

/* Reading DATA pins                                                         */
#define LCD_DATA_IN           ((IOPIN1 >> 24) & 0xF)

/* Zapis wartosci na liniach DB4 - DB7                                       */
#define LCD_DATA_OUT(x)       IOCLR1 = PINS_DATA; IOSET1 = (x & 0xF) << 24;

/* Ustawienie wybranych pin�w portu jako wyjscia                             */
#define LCD_ALL_DIR_OUT       IODIR1  |=  PINS_CTRL | PINS_DATA;

/* Ustawienie wybranych pin�w jako wejscia                                   */
#define LCD_DATA_DIR_IN       IODIR1 &= ~PINS_DATA;

/* Ustawienie wybranych pin�w jako wyjscia                                   */
#define LCD_DATA_DIR_OUT      IODIR1 |=  PINS_DATA;

/*****************************************************************************/
/* prototypy uzytych funkcji                                                 */
/*****************************************************************************/
extern void lcd_init    (void);
extern void lcd_clear   (void);
extern void lcd_putchar (char c);
extern void set_cursor  (unsigned char column, unsigned char line);
extern void lcd_print   (unsigned char const *string);


#endif
