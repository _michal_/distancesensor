/******************************************************************************/
/* LCD.c: Procedura obslugi wyswietlacza 2x16 ze sterownikiem HD44780         */
/******************************************************************************/

#include <LPC23xx.H>                     /* biblioteka nagl�wkowa procesora LPC23xx              */
#include "LCD.h"

/************************ Funkcje obslugi wyswietlacza 2x16 *******************/


/*******************************************************************************
* 1. Funkcja opozniajaca                                                  *
*   Parameter:    cnt:    number of while cycles to delay                      *
*******************************************************************************/

static void delay (int cnt)
{
	 int i; 
	 for (i = 0; i < cnt; i++);
}


/*******************************************************************************
* 2. Funkcja wysylajaca dane do kontrolera LCD                                 *
*   Parameter:    c:      dana do zapisu                                       *
*******************************************************************************/

void lcd_write_4bit (unsigned char c)
{
  LCD_RW(0)
  LCD_E(1)
  LCD_DATA_OUT(c&0x0F)
  delay(10);
  LCD_E(0)
  delay(10);
}


/*******************************************************************************
* 3. Funkcja zapisujaca polecenia do konrolera LCD                             *
*   Parameter:    c:      polecenie do zapisu                                  *
*******************************************************************************/

void lcd_write_cmd (unsigned char c)
{
  delay (8100);
  LCD_RS(0)
  lcd_write_4bit (c>>4);
  lcd_write_4bit (c);
}


/*******************************************************************************
* 4. Funkcja zapisujaca dane do konrolera LCD                                                *
*   Parameter:    c:      data to be written                                   *
*******************************************************************************/

static void lcd_write_data (unsigned char c)
{
  delay (5100);
  LCD_RS(1)
  lcd_write_4bit (c>>4);
  lcd_write_4bit (c);
}


/*******************************************************************************
* 5. Funkcja wysylajaca pojedynczy znak do kontrolera LCD                      *
*   Parameter:    c:      character to be printed                              *
*******************************************************************************/

void lcd_putchar (char c)
{ 
  lcd_write_data (c);
}


/*******************************************************************************
* 6. Procedura inicjalizacji LCD w tryb 4-bitowy                               *
*   Parameter:                                                                 *
*******************************************************************************/

void lcd_init (void)
{ 

  /* Ustawienie pin�w jako wyjscia                                            */
  LCD_ALL_DIR_OUT

  delay (1520);
  LCD_RS(0)
  lcd_write_4bit (0x3);                 /* Select 4-bit interface             */
  delay (4100);
  lcd_write_4bit (0x3);
  delay (100);
  lcd_write_4bit (0x3);
  lcd_write_4bit (0x2);
  delay (100);
  lcd_write_cmd (0x28);                 /* 2 lines, 5x8 character matrix      */
  lcd_write_cmd (0x0C);                 /* Display ctrl:Disp=ON,Curs/Blnk=OFF */
  lcd_write_cmd (0x06);                 /* Entry mode: Move right, no shift   */

}

/*******************************************************************************
* 7. Ustawienie kursora w dowolnej pozycji                                     *
*   Parameter:   column - kolumna (1 lub 2)                                    *
*                 line - linia   (od 1 do 16)                                  *
*******************************************************************************/

void set_cursor (unsigned char column, unsigned char line)
{
  unsigned char address;

  address = (line * 40) + column;
  address = 0x80 + (address & 0x7F);
  lcd_write_cmd(address);               /* Set DDRAM address counter to 0     */
}

/*******************************************************************************
* 8. Polecenie czyszczenia LCD                                                 *
*******************************************************************************/

void lcd_clear (void)
{
  lcd_write_cmd(0x01);                  /* Display clear                      */
  set_cursor (0, 0);
}


/*******************************************************************************
* 9. Zapis lancucha znak�w na wyswietlaczu LCD                                 *
*   Parameter: wskaznik do lancucha znak�w                                     *
*   Return:                                                                    *
*******************************************************************************/

void lcd_print (unsigned char const *string)
{
  while (*string)  {
    lcd_putchar (*string++);
  }
}

/******************************************************************************/
