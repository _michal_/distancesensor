#include "headers.h"

/* echo signal P0.29 
   transmitter signal P0.11
	 prescale 12 because of no PLL
	 BUTTON_START pin 0.16 - start measurment
	 BUTTON_CLEAR pin 0.17 - clear data 
	 It's not safe to press clear and start_measurment buttons at the same time
	 
	 there can be problems with pull ups and pin settings*/
	 
int main(void)
{
	/******************************SENSOR******************************************/
	PINSEL0 = 0x00; // port 0 set as GPIO
	IODIR0 |= TRIG; //port 0.11 set as output
	IOCLR0 |= TRIG; // send 0 at the start, change on high in fucntion send signal
	PINSEL1 = 0x00; // port 1 set as GPIO
	IODIR1 &= ~ECHO; // port 1.29 as input
	unsigned int timeOfEcho = 0; //time for which there is high on pin echo 
	float distanceFromSensor = 0;
	unsigned char distanceString[10];
	/*******************************************************************************/
	/***************************BUTTON**********************************************/
	int allTimeStateStart, allTimeStateClear, startMeasurment, clear;
	IODIR0 &= ~(BUTTON_START); 
	char * textA = "Rozpocznij ";
	char * textB = "pomiar";
	
	delayMicroS(100); //wait until state is set on pins
	
	allTimeStateStart = IOPIN0 & BUTTON_START; //1
	allTimeStateClear = IOPIN0 & BUTTON_CLEAR; //1
	clear = IOPIN0 & BUTTON_CLEAR; //1
	/*******************************************************************************/
	/*************************LCD***************************************************/
	lcd_init();
	/*******************************************************************************/
	while(1)
	{
		startMeasurment = IOPIN0 & BUTTON_START; //1
		
		if(startMeasurment == allTimeStateStart) 
		{
			clear = IOPIN0 & BUTTON_CLEAR;
			lcd_clear();
			set_cursor(0, 1);
			lcd_print((unsigned char *)textA);
			set_cursor(0, 2);
			lcd_print((unsigned char *)textB);	
			
			while(startMeasurment == allTimeStateStart)
			{
				startMeasurment = IOPIN0 & BUTTON_START;
			}
		}
		else
		{
			clear = IOPIN0 & BUTTON_CLEAR;
			sendSignal(); // send signal for 10us
			timeOfEcho = timeToCountDistance(); //value of time which passed to receive echo signal	
			distanceFromSensor = distance(timeOfEcho);
			sprintf((char *)distanceString, "%.2f", distanceFromSensor);
			lcd_clear();
			set_cursor(0, 1);
			lcd_print(distanceString);
			
			while(clear == allTimeStateClear) //wait untill clear is not press on
			{
				clear = IOPIN0 & BUTTON_CLEAR;
			}
		}
	}
	return 0;
}
