#include <LPC23xx.H> 
#include "LCD.h"
#include <stdio.h>
#define PRESCALE 12
#define TRIG (1<<11)
#define ECHO (1<<29)
#define BUTTON_START (1<<16)
#define BUTTON_CLEAR (1<<17)


void initTimer(int prescale);
void delayMicroS(unsigned int microseconds); //it's created to microsecond because [uS] are required for start transmitter
																							//how much microseconds is set up by prescale			
unsigned int timeToCountDistance(void);																						
float distance(unsigned int numberOfClocks);
void sendSignal(void);
void startMeasurment(void);
