#include "headers.h"

void sendSignal(void) //this function sends signal from sensor for 10uS
{
	IOSET0 |= TRIG; // gpio 0.11 as high
	delayMicroS(10);		// sent high for 10uS
	IOCLR0 |= TRIG; //gpio 0.11 as low
	
	return;
}
