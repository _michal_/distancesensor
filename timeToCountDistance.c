#include "headers.h"

unsigned int timeToCountDistance(void)
{
		unsigned int timeMicroS = 0;
		
		while(!(IOPIN1 & ECHO)) {} // stop function until signal is back	
			
		if((IOPIN1 & ECHO))
		{
			initTimer(PRESCALE);
		}
		
		while((IOPIN1 & ECHO)) {} //PORT P1.10 IS AN ECHO SIGNAL, stop function till it's high
		timeMicroS = T0TC;
		T0TCR = 0x00; //counter off
		
		return timeMicroS; //after this clocks the back-signal is received
}
