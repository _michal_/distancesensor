#include "headers.h"

void delayMicroS(unsigned int microseconds)
{
	initTimer(PRESCALE);
	while(T0TC < microseconds) {}	
	T0TCR = 0x00; //counter off
	
	return;
}
