#include "headers.h"


void initTimer(int prescale)
{
	T0CTCR = 0x00;  //select counter mode
	T0PR = prescale - 1;  //-1 because counting starts from 0. time for 1 uS
	T0TCR = 0x02; //zeros on prescale register and time counter register
	T0TCR = 0x01; //counter enabled
	
	return;
}
